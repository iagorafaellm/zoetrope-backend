/* Title can be Movies, Series, Animes and so on */

const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Title = sequelize.define('Title', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    genres: {
        type: DataTypes.STRING,
        allowNull: false
    },

    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    release_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    
    mean_score: {
        type: DataTypes.INTEGER
    },
    
    director: {
        type: DataTypes.STRING
    },
});

Title.associate = function(models) {
    Title.belongsTo(models.User);
}

module.exports = Title;