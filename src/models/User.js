const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_birth: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING
    },
    
    country: {
        type: DataTypes.STRING
    },
    
    favorite_genre: {
        type: DataTypes.STRING
    },
});

User.associate = function(models) {
    User.hasMany(models.Title);
    User.belongsTo(models.Role);
}

module.exports = User;