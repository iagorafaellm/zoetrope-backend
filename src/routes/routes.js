const { Router } = require('express');
const UserController = require('../controllers/UserController');
const RoleController = require('../controllers/RoleController');
const TitleController = require('../controllers/TitleController');


const router = Router();

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

router.put('/usersaddrole/:id', UserController.addRelationRole);
router.delete('/usersremoverole/:id', UserController.removeRelationRole);


router.get('/roles', RoleController.index);
router.post('/roles', RoleController.create);


router.get('/titles', TitleController.index);
router.get('/titles/:id', TitleController.show);
router.post('/titles', TitleController.create);
router.put('/titles/:id', TitleController.update);
router.delete('/titles/:id', TitleController.destroy);

module.exports = router;
