const { response } = require('express');
const User = require('../models/User');
const Role = require('../models/Role');

const index = async(req,res) => {
    try {
        const roles = await Role.findAll();
        return res.status(200).json({roles});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try {
          const role = await Role.create(req.body);
          return res.status(201).json({message: "Papel cadastrado com sucesso!", role: role});
      } catch(err) {
          res.status(500).json({error: err});
      }
};

const addRelationRole = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const role = await Role.findByPk(req.body.RoleId);
        await user.setRole(role);
        return res.status(200).json(user);
    } catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    create
};
