const { response } = require('express');
const Title = require('../models/Title');

const index = async(req,res) => {
    try {
        const titles = await Title.findAll();
        return res.status(200).json({titles});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const title = await title.findByPk(id);
        return res.status(200).json({user});
    } catch(err) {
        return res.status(500).json({err});
    }
};

const create = async(req,res) => {
    try {
          const title = await Title.create(req.body);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", title: title});
      } catch(err) {
          res.status(500).json({error: err});
      }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Title.update(req.body, {where: {id: id}});
        if(updated) {
            const title = await Title.findByPk(id);
            return res.status(200).send(title);
        } 
        throw new Error();
    } catch(err) {
        return res.status(500).json("Usuário não encontrado!");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Title.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso!");
        }
        throw new Error ();
    } catch(err) {
        return res.status(500).json("Usuário não encontrado!");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};
